import { fireEvent, render, screen } from "@testing-library/react"
import Button from "./components/Button";
import Modal from "./components/Modal";
import { openDeleteModalAction, closeDeleteModalAction, modalsReducer, openAddModalAction, closeAddModalAction} from "./store/modalsReducer";
import { fetchProductsAction, productsReducer } from "./store/productsReducer";
import renderer from 'react-test-renderer';
import Header from "./components/header/Header";
import { MemoryRouter } from "react-router-dom";
import FormToBuyProduct from "./components/formToBuyProduct/FormToBuyProduct";
import { Provider } from "react-redux";
import configureStore from 'redux-mock-store';

const modal = (handleClick, closeModal) => {
  return <Modal
  backgroundColor='green'
  header='Modal window header'
  closeButton='true'
  text='Modal window main text'
  actions={<><button className='buttonForAddModal' onClick={handleClick}>Add</button>
  <button className='buttonForAddModal' onClick={closeModal}>No</button></>}
  closeModal={closeModal}
/>
}

test("Button add to cart and delete from cart test", () => {
    const handleClick = jest.fn()
    render(<Button backgroundColor={"green"} text="Add to cart" onClick={handleClick}/>)
    const buttonElement = screen.getByText(/Add to cart/)
    expect(buttonElement).toBeInTheDocument()
    expect(buttonElement.style.backgroundColor).toBe("green")
    fireEvent.click(buttonElement)
    expect(handleClick).toHaveBeenCalledTimes(1)
})

describe("Modal test", () => {
  const handleClick = jest.fn()
  const closeModal = jest.fn()

  test("Modal windows test", () => {
    render(modal(handleClick, closeModal))
    const modalWindow = screen.getByText("Modal window header")
    expect(modalWindow).toBeInTheDocument()
    const modalContainer = screen.getByTestId("modalContainer")
    expect(modalContainer.style.backgroundColor).toBe("green")
    // Перевірка наявності кнопок
    const buttons = screen.getAllByRole("button")
    for (let button of buttons) {
      expect(button).toBeInTheDocument()
    }
  })

  test("Modal closing test", () => {
    render(modal(handleClick, closeModal))
    // Тестимо закриття модального вікна
    const modalWrapper = screen.getByTestId("modalWrapper")
    fireEvent.click(modalWrapper)
    expect(closeModal).toBeCalledTimes(1)
  })

  test("Modal closing with cross icon", () => {
    render(modal(handleClick, closeModal))
    // Перевірка виконання функції при кліку на іконку хрестика модального вікна
    const crossIcon = screen.getByTestId("crossIcon")
    expect(crossIcon).toBeInTheDocument()
    fireEvent.click(crossIcon)
    expect(closeModal).toBeCalledTimes(1)
  })

  test("Modal buttons click triggering", () => {
    render(modal(handleClick, closeModal))
    // Перевірка виконання функції при кліку на кнопки модального вікна
    const NoButton = screen.getByText("No")
    fireEvent.click(NoButton)
    expect(closeModal).toBeCalledTimes(1)
    const AddButton = screen.getByText("Add")
    fireEvent.click(AddButton)
    expect(handleClick).toBeCalledTimes(1)
  })

  afterEach(() => {
    jest.clearAllMocks()
  })
  
})

// ----- Тести для редюсерів ----- //

// modalsReducer test
describe("Modals reducer test", () => {
  const defaultState =  {
    addModalState: false,
    deleteModalState: false
}
  test("Reducer returns true state of modals", () => {
    // openAddModalAction
    let state = modalsReducer(defaultState, openAddModalAction())
    expect(state).toEqual({
      addModalState: true,
      deleteModalState: false
    })
    // closeAddModalAction
    state = modalsReducer(defaultState, closeAddModalAction())
    expect(state).toEqual({
      addModalState: false,
      deleteModalState: false
    })

    // openDeleteModalAction
    state = modalsReducer(defaultState, openDeleteModalAction())
    expect(state).toEqual({
      addModalState: false,
      deleteModalState: true
    })
    // closeDeleteModalAction
    state = modalsReducer(defaultState, closeDeleteModalAction())
    expect(state).toEqual({
      addModalState: false,
      deleteModalState: false
    })
  })
})

// productsReducer test
describe("Products reducer test", () => {

  test("Products reducer handles fetching products", async () => {
    const fetchUser = await fetch("https://jsonplaceholder.typicode.com/users/1")
    const user = await fetchUser.json()

    const defaultState = []
    const reducerReturn = productsReducer(defaultState, fetchProductsAction(user))
    expect(reducerReturn).toEqual({
      "id": 1,
      "name": "Leanne Graham",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    })

  })
})

// -----  snapshots ----- //

// -----  Header snapshot----- //
test("Header renders correctly", () => {
  const tree = renderer
    .create(
    <MemoryRouter>
      <Header />
    </MemoryRouter>
    )
    .toJSON();
  
  expect(tree).toMatchSnapshot()
})

// ----- Form to buy products snapshot ----- //
test("Form to buy products renders correctly", () => {
  const middlewares = []
  const mockStore = configureStore(middlewares)
  const defaultState = {
    productsInCart: []
  }
  const store = mockStore(defaultState)

  const tree = renderer
    .create(
      <Provider store={store}>
        <FormToBuyProduct/>
      </Provider>
    )
    .toJSON();
  
    expect(tree).toMatchSnapshot()
})