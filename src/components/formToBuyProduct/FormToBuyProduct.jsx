import { ErrorMessage, Field, Form, Formik } from "formik";
import style from './FormToBuyProduct.module.scss'
import * as Yup from 'yup'
import { useDispatch, useSelector } from "react-redux";

import { deleteAllProductsInCartAction } from "../../store/productsInCartReducer";
// Імпортуємо компонент з react-number-format
import { PatternFormat } from 'react-number-format';

const ValidationSchema = Yup.object().shape({
    name: Yup.string()
        .required("Enter your name"),
    surname: Yup.string()
        .required("Enter your surname"),
    age: Yup.number()
        .required("Enter your age")
        .min(14, "Inappropriate age")
        .max(100, "Inappropriate age"),
    address: Yup.string()
        .required("Enter your address"),
})  

let phoneNumberValue = ""

const formikChildComponent = ({values, errors, handleChange, handleBlur}) => {

    return (
        <Form>
            <div className={style.allInputsContainer}>
                <div className={style.inputWrapper}>
                    <span className={style.inputTitle}>Name</span>
                    <Field type="text" name="name" className={style.inputStyle} />
                    <ErrorMessage name="name" component={'span'} className={style.errorMessage}/>
                </div>
                <div className={style.inputWrapper}>
                    <span className={style.inputTitle}>Surname</span>
                    <Field type="text" name="surname" className={style.inputStyle}/>
                    <ErrorMessage name="surname" component={'span'} className={style.errorMessage}/>
                </div>
                <div className={style.inputWrapper}>
                    <span className={style.inputTitle}>Age</span>
                    <Field type="text" name="age" className={style.inputStyle}/>
                    <ErrorMessage name="age" component={'span'} className={style.errorMessage}/>
                </div>
                <div className={style.inputWrapper}>
                    <span className={style.inputTitle}>Address</span>
                    <Field type="text" name="address" className={style.inputStyle}/>
                    <ErrorMessage name="address" component={'span'} className={style.errorMessage}/>
                </div>
                <div className={style.inputWrapper}>
                    <span className={style.inputTitle}>Phone number</span>
                    <PatternFormat
                        name="phoneNumber"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={style.inputStyle} 
                        format="+(380) ##-###-####" 
                        allowEmptyFormatting 
                        mask="_" 
                        onValueChange={(val) => {
                            values.phoneNumber = val.value;
                            phoneNumberValue = val.value;
                        }}
                        />
                    <ErrorMessage name="phoneNumber" component={'span'} className={style.errorMessage}/>
                </div>
            </div>

            <button type="submit" className={style.ckeckoutButton}>Checkout</button>
        </Form>
    )
}

function FormToBuyProduct({deleteAllProductsInCart}) {
    const productsInCart = useSelector(state => state.productsInCart)
    console.log(productsInCart);
    const dispatch = useDispatch()

    function getSelectedProducts(){
        let addedToCartProductsId = JSON.parse(localStorage.getItem('addedToCartProducts'));
        fetch('products.json')
        .then(res => res.json())
        .then(data => {
            console.log("ЗАМОВЛЕНІ ТОВАРИ", [...data.products].filter(product=>
                addedToCartProductsId.includes(product.id)
            ))
        })
        .catch(error => console.log('An error occured while fetching added to cart products: ', error))
    }

    return (
        <Formik
            initialValues={{name: '', surname: '', age: '', address: '', phoneNumber: ''}}
            validate={values => {
                const errors = {};
                
                // Перевірка чи містить ім'я числові значення
                for (let i=0; i<=9; i++){
                    if (values.name.includes(i)) {
                        errors.name = "Inappropriate name"
                    }
                }

                // Перевірка чи містить прізвище числові значення
                for (let i=0; i<=9; i++){
                    if (values.surname.includes(i)) {
                        errors.surname = "Inappropriate surname"
                    }
                }

                // Перевіряємо чи користувач повністю заповнив поле вводу для телефону
                // Для цього потрібно скористатися саме змінною phoneNumberValue, 
                // а не values.phoneNumber
                if (phoneNumberValue.length < 9){
                    console.log(phoneNumberValue);
                    console.log(values.phoneNumber.length);
                    console.log("ERRORS", errors);
                    errors.phoneNumber = "Enter full phone number"
                }

                return errors;
            }}
            onSubmit={values => {
                // values.phoneNumber = phoneNumberValue;
                // Відображаємо в консолі дані, введенні у форму користувачем
                console.log("ДАНІ ФОРМИ", values)
                // Відображаємо в консолі товари, що замовили 
                getSelectedProducts()
                // Викликаємо модальне вікно, щоб повідомити користувача, що його замовлення
                // відправлене
                alert("Your order has been sent!")
                // Видаляємо з локального сховища addedToCartProducts
                dispatch(deleteAllProductsInCartAction())
                // Змінну productsInCart в компоненті Cart змінюємо на пустий масив, 
                // щоб сторінка перерендерилася
                deleteAllProductsInCart()
            }}
            validationSchema={ValidationSchema}
        >
            {formikChildComponent}
        </Formik>
    );
}

export default FormToBuyProduct;