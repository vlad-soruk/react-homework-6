import { useContext, useEffect, useRef, useState } from 'react';
import styles from './ProductCard.module.scss';
import PropTypes from 'prop-types';
import { ProductsView } from '../../App';

ProductCard.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imgUrl: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    setNumber: PropTypes.string.isRequired,
    selectedProductsId: PropTypes.array,
    updateSelectedProductsCount: PropTypes.func,
    openAddModal: PropTypes.func,
    openDeleteModal: PropTypes.func,
    deleteButton: PropTypes.bool,
    selectedProductsPage: PropTypes.bool,
}

ProductCard.defaultProps = {
    selectedProductsId: [],
    updateSelectedProductsCount: ()=>{},
    openAddModal: ()=>{},
    // Вказуємо, що по дефолту, якщо не передавався пропс deleteButton
    // то він буде false
    deleteButton: false,
    selectedProductsPage: false,
    openDeleteModal: ()=>{},
}

function ProductCard(props){
    // Використовуємо useContext та забираємо інформацію про відображення
    // товарів (табличне або карточне) й функцію зміни вигляду товарів
    const {productsViewMode, changeProductsView} = useContext(ProductsView)

    let {id, name, price, description, imgUrl, color, openAddModal, openDeleteModal, selectedProductsId, updateSelectedProductsCount, deleteButton, selectedProductsPage, deleteSelectedProduct} = props;
    const [productSelected, setProductSelected] = useState(false);
    let selectedProducts = useRef([]);
    let selectedProdsId = useRef(selectedProductsId)

    // На стадії монтування повинен спрацювати колбек хука useEffect, який 
    // дістане зі сховища список обраних товарів. Якщо ж в сховищі нічого не має,
    // то selectedProducts.current буде пустим списком (за замовчуванням)
    useEffect(() => {
        if (localStorage.getItem('selectedProducts')) {
            selectedProducts.current = JSON.parse(localStorage.getItem('selectedProducts'));
            selectedProdsId.current = JSON.parse(localStorage.getItem('selectedProducts'));
            console.log('INITIAL selectedProductsId ARRAY: ', selectedProdsId.current);
            console.log("Initial selectedProducts.current array ", selectedProducts.current);
            // Якщо айді вже знаходиться в локальному сховищі, тобто товар вже 
            // обраний, то ставимо productSelected цієї карточки в true 
            // щоб потім при кліку на іконку цей товар видалився з обраних 
            if (selectedProdsId.current.includes(id)) {
                setProductSelected(true);
            }
        }
        else {
            selectedProducts.current = [];
            console.log("Initial selectedProducts.current array ", selectedProducts.current);
        }
    }, [id]);

    function selectProduct() {
        if (!productSelected) {
            // Записуємо в selectedProducts.current актуальні дані з локал стореджу
            // при кожному кліці на іконку додавання товару в обрані
            if (localStorage.getItem('selectedProducts')){
                selectedProducts.current = JSON.parse(localStorage.getItem('selectedProducts'));
            }
            // Далі додаємо в цей список з айді обраних товарів новий айді обраного товару
            selectedProducts.current.push(id);
            localStorage.setItem('selectedProducts', JSON.stringify(selectedProducts.current));
            setProductSelected(true);
            // Також змінюємо пропс, який прийшов з App.js, щоб тримати там актуальні айді 
            // оскільки базуючись на айді з selectedProdsId.current та selectedProducts.current
            // svg іконка буде в активному стані (вибраному) чи звичайному (невибраному)
            selectedProdsId.current = JSON.parse(localStorage.getItem('selectedProducts'));
            // Оновлюємо кількість обраних товарів, що показуватиметься у компоненті Header
            updateSelectedProductsCount();
            console.log("selectedProducts.current after product wasn`t selected ", selectedProducts.current);
            console.log('SelectedProdsId ARRAY after product wasn`t selected: ', selectedProdsId.current);
        } else {
            // Якщо вже був клік по іконці додавання товару до обраних, 
            // дістаємо з локал стореджу актуальний список айді обраних товарів та 
            // записуємо його в selectedProducts.current
            selectedProducts.current = JSON.parse(localStorage.getItem('selectedProducts'));
            // Так як фільтр повертає новий масив, а не мутує старий,
            // нам потрібно спочатку записати результат фільтрації в змінну updatedSelectedProducts
            // а потім ототожнити його з selectedProducts.current
            let updatedSelectedProducts = selectedProducts.current.filter(product => product !== id);
            console.log(selectedProducts.current);
            selectedProducts.current = updatedSelectedProducts;
            console.log("Filtrated selectedProducts.current after product was selected ", selectedProducts.current)
            localStorage.setItem('selectedProducts', JSON.stringify(updatedSelectedProducts));
            setProductSelected(false);
            console.log("selectedProducts.current after product was selected ", selectedProducts.current);
            // Після всіх маніпуляцій з локальним сховищем оновлюємо selectedProdsId.current
            selectedProdsId.current = JSON.parse(localStorage.getItem('selectedProducts'));
            // Оновлюємо кількість обраних товарів, що показуватиметься у компоненті Header
            updateSelectedProductsCount();
            console.log("SelectedProdsId array after product was selected: ", selectedProdsId.current);
        }
    }

    return (
        // В залежності від значення productsViewMode змінюємо стилі для діва
        <div className={`${styles.productWrapper} ${productsViewMode==="cards" ? "" : styles.tableView}`}>
            <div className={productsViewMode==="cards" ? styles.productInfoContainer : styles.productInfoContainerTableView}>
                <div className={styles.imgWrapper}>
                    <img className={styles.productImage} src={imgUrl} alt="product" />
                </div>
                <h1 className={productsViewMode==="cards" ? styles.productName : styles.productNameTableView}>{name}</h1>
                <p className={productsViewMode==="cards" ? styles.productDescription : styles.productDescriptionTableView}>{description}</p>
                <p className={productsViewMode==="cards" ? styles.productColor : styles.productColorTableView}>Color: <span>{color}</span></p>
            </div>

            <div className={productsViewMode==="cards" ? styles.priceAndBtnWrapper : styles.priceAndBtnWrapperTableView}>
                {deleteButton 
                    ? <>
                        <p className={styles.productPrice}>${price}</p>
                        <button className={productsViewMode==="cards" ? styles.deleteBtn : styles.deleteBtnTableView} onClick={(event)=>openDeleteModal(event, id)}>
                            Delete from cart
                        </button>
                    </>

                    : <>
                        {/* Якщо це не сторінка корзини (тобто пропс deleteButton===false),
                        а це сторінка вибраних товарів (тобто пропс selectedProductsPage===true),
                        то ми не показуємо кнопку 'Add to cart'*/}
                        { selectedProductsPage 
                            ? <>
                                <p className={styles.productPrice}>${price}</p>
                            </>
                            : <>
                                <p className={styles.productPrice}>${price}</p>
                                <button className={productsViewMode==="cards" ? styles.buyBtn : styles.buyBtnTableView} onClick={(event)=>openAddModal(event, id)}>
                                    Add to cart
                                </button>
                            </>
                        }
                            <svg onClick={selectedProductsPage ? ()=>deleteSelectedProduct(id) : selectProduct}
                                // Якщо айді картки вже є в локальному сховищі вибраних товарів або товар став вибраним (після
                                // натиснення користувачем на зірочку), іконка буде мати стиль вибраної
                                className={selectedProdsId.current.includes(id) || productSelected ? `${productsViewMode==="cards" ? styles.starIcon : styles.starIconTableView} ${styles.selected}` : productsViewMode==="cards" ? styles.starIcon : styles.starIconTableView }
                                version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 47.94 47.94">
                            <path d="M26.285,2.486l5.407,10.956c0.376,0.762,1.103,1.29,1.944,1.412l12.091,1.757c2.118,0.308,2.963,2.91,1.431,4.403l-8.749,8.528c-0.608,0.593-0.886,1.448-0.742,2.285l2.065,12.042c0.362,2.109-1.852,3.717-3.746,2.722l-10.814-5.685c-0.752-0.395-1.651-0.395-2.403,0l-10.814,5.685c-1.894,0.996-4.108-0.613-3.746-2.722l2.065-12.042c0.144-0.837-0.134-1.692-0.742-2.285l-8.749-8.528c-1.532-1.494-0.687-4.096,1.431-4.403l12.091-1.757c0.841-0.122,1.568-0.65,1.944-1.412l5.407-10.956C22.602,0.567,25.338,0.567,26.285,2.486z"/>
                            </svg>
                    </>
                }
            </div>
        </div>
    )
}

export default ProductCard