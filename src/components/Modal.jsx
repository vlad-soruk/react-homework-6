import styles from './DeleteModal.module.scss'
function Modal({backgroundColor, header, closeButton, text, closeModal, actions}){
    return (
        <div data-testid="modalWrapper" className={styles.modalWrapper} onClick={(e)=>{
            if (e.target.className.includes('modalWrapper')) {
                closeModal();
            }
        }}>
            <div data-testid="modalContainer"  style={{background: backgroundColor}} className={styles.modalContainer}>
                <div className={styles.titleContainer}>
                    <h1 className={styles.title}>{header}</h1>
                    {closeButton==='true'
                        && <span 
                                data-testid="crossIcon"
                                className={styles.crossIcon}
                                onClick={closeModal}
                                >&#128937;
                            </span>
                    }
                </div>
                <p className={styles.mainText}>
                    {text}
                </p>
                <div className={styles.buttonWrapper}>
                    {actions}
                </div>
            </div>
        </div>
    )
}

export default Modal