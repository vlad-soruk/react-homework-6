import { useContext, useEffect, useRef, useState } from 'react';
import ProductCard from '../../components/productCard/ProductCard';
import { ProductsView } from '../../App';
import Button from '../../components/Button';

function Favorites({updateSelectedProductsCount}){
    // Використовуємо useContext для того, щоб компонент міг використовувати необхідні для
    // реалізації відображення карток різним способом дані 
    const {productsViewMode, changeProductsView} = useContext(ProductsView)

    let [selectedProducts, setSelectedProducts] = useState([]);
    // За замовчуванням вказуємо, що стан коли немає вибраних товарів дорівнює false
    let [noSelectedProducts, setNoSelectedProducts] = useState(false)
    // Тут зберігатимемо айді вибраних товарів
    const selectedProductsIdArray = useRef([]);

    // Функція видалення товару з обраного, що спрацює в компоненті ProductCard
    // при натиснені на іконку зірочки
    function deleteSelectedProduct(id) {
        // Якщо користувач вирішив видалити товар з обраних, фільтруємо список айді,
        // що знаходиться в selectedProductsIdArray.current та оновлюємо список вибраних
        // товарів в локальному сховищі 
        let updatedSelectedProductsId = [...selectedProductsIdArray.current].filter(productId => productId!==id);
        localStorage.setItem('selectedProducts', JSON.stringify(updatedSelectedProductsId));
        // Змінюємо стан selectedProducts чим викликаємо ререндер сторінки
        let updatedSelectedProducts = selectedProducts.filter(product => product.id !== id);
        setSelectedProducts(updatedSelectedProducts);
        // Викликаємо функцію, що оновлює кількість товарів, доданих у кошик, що показується на сторінці
        updateSelectedProductsCount();
    }

    useEffect(()=>{
        let selectedProductsId = JSON.parse(localStorage.getItem('selectedProducts'));
        selectedProductsIdArray.current = selectedProductsId;
        // Якщо немає доданих у вибране товарів, то змінюємо стан noSelectedProducts на true
        if(selectedProductsId.length === 0){
            setNoSelectedProducts(true)
        }
        console.log("SELECTED PRODUCTS ID: ", selectedProductsIdArray.current);
        // Не забуваємо додати selectedProducts в список залежностей useEffect
    }, [selectedProducts])

    useEffect(()=>{
        fetch('products.json')
        .then(res => res.json())
        .then(data => {
            // Додаємо у змінну selectedProducts лише ті товари, айді яких присутнє в
            // масиві selectedProductsIdArray
            setSelectedProducts([...data.products].filter(product=>
                selectedProductsIdArray.current.includes(product.id)
            ))
        })
        .catch(error => console.log('An error occured while fetching selected products: ', error))
    }, [])

    return (
        <>
             {/* Кнопка, що перемикає вигляд відображення товару */}
            <div className="changeViewButtonWrapper">
                <Button
                    backgroundColor={productsViewMode==="cards" ? "purple": "#ff9187"}
                    text={`Change view to ${productsViewMode==="cards" ? "table" : "cards"}`}
                    onClick={changeProductsView}
                />
            </div>

            <div className="cardsWrapper">
                {/* При noSelectedProducts === true відображаємо надпис що немає товарів*/}
                {noSelectedProducts
                ?   <h1 className='noProductsTitle'>There is no selected products yet</h1>
                :   null
                }
                {selectedProducts.map(product => <ProductCard
                                                    key={product.setNumber}
                                                    id={product.id}
                                                    name={product.name}
                                                    price={product.price}
                                                    description={product.description}
                                                    color={product.color}
                                                    imgUrl={product.imgUrl}
                                                    selectedProductsPage={true}
                                                    deleteSelectedProduct={deleteSelectedProduct}
                                                />)}
            
            </div>
        </>
    )
}

export default Favorites