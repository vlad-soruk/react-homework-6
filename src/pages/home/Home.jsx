import { useRef, useEffect, useContext } from "react";
import Button from "../../components/Button";
import Modal from "../../components/Modal";
import ProductCard from "../../components/productCard/ProductCard"
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from "../../asyncActions/fetchProducts";
import { closeAddModalAction, openAddModalAction } from "../../store/modalsReducer";
import { ProductsView } from "../../App";

function Home({updateAddedToCartProductsCount, updateSelectedProductsCount}) {
    // Використовуємо useContext для того, щоб компонент міг використовувати необхідні для
    // реалізації відображення карток різним способом дані 
    const {productsViewMode, changeProductsView} = useContext(ProductsView)

    // const [isAddModalOpen, setIsAddModalOpen] = useState(false);
    const selectedProductsId = useRef([]);
    const addedToCartProductsId = useRef([]);

    const products = useSelector(state => state.products)
    const isAddModalOpen = useSelector(state => state.modals.addModalState)
    const dispatch = useDispatch();

    function handleAddToCart(){
        closeModal();
        console.log(addedToCartProductsId);
        let alreadyAddedToCartProducts = [];

        if (localStorage.getItem('addedToCartProducts')) {
            alreadyAddedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts'));
            if(alreadyAddedToCartProducts.includes(addedToCartProductsId.current)) {
                alert('You`ve already added this product to a cart!');
                return
            }
        }
        localStorage.setItem('addedToCartProducts', JSON.stringify([...alreadyAddedToCartProducts, addedToCartProductsId.current]));
        // Оновлюємо кількість доданих у кошик товарів
        updateAddedToCartProductsCount();
    }

    useEffect(()=>{
        let selectedProductsFromStorage = [];
        // Отримуємо айді обраних товарів з локал стореджу
        // Будемо передавати це значення в пропси до компоненту ProductCard
        // аби додані в обране товари зберігалися після перезавантаження сторінки
        if(localStorage.getItem('selectedProducts')){
            selectedProductsFromStorage = JSON.parse(localStorage.getItem('selectedProducts'));
        }
        console.log('Функция selectedProductsFromStorage сработала');
        selectedProductsId.current = selectedProductsFromStorage;
        console.log(selectedProductsFromStorage);
        console.log(selectedProductsId.current);
    }, [])

    // Використовуємо хук useRef щоб зберігати значення скролу сторінки
    // useState ми не можемо використати, бо функція setState має асихронну природу,
    // тому ми отримуватимемо старі (минулі) значення scrollY, тоді як useRef
    // дозволяє зберігати тільки актуальне значення
    const scrollY = useRef(0); 

    function openAddModal(event, id){
        scrollY.current = window.scrollY;
        dispatch(openAddModalAction())
        // setIsAddModalOpen(true);
        document.body.style.position = 'fixed';
        // Важливо вказати, що ширина дорівнюватиме 100%,
        // інакше ширина body буде меншою, ніж сам екран
        document.body.style.width = '100%';
        // Вказуємо, що box-sizing = 'border-box'
        // для того, щоб падінги враховувались у ширині body
        document.body.style.boxSizing = 'border-box';
        document.body.style.top = `-${scrollY.current}px`;
        document.body.style.paddingRight = '17px';
        
        //Зберігаємо айді товару, на якому визвали модальне вікно додавання товару в кошик
        addedToCartProductsId.current = id;
        console.log(addedToCartProductsId.current);
    }

    function closeModal(){
        dispatch(closeAddModalAction())
        // setIsAddModalOpen(false);
        document.body.style.position = '';
        document.body.style.top = ``;
        document.body.style.width = '';
        document.body.style.boxSizing = '';
        document.body.style.top = `-${scrollY.current}px`;
        window.scrollTo(0, scrollY.current);
        document.body.style.paddingRight = '';
    }

    useEffect(()=>{
        // Використовуємо диспатч та передаємо в нього асинхронну функцію,
        // що в свою чергу повертає функцію з параметром диспатч, дістає з json файла
        // товари та змінює стан масиву даних з товарами
        dispatch(fetchProducts())
    }, [dispatch])

    return (
        <>
            {/* Кнопка, що перемикає вигляд відображення товару */}
            <div className="changeViewButtonWrapper">
                <Button
                    backgroundColor={productsViewMode==="cards" ? "purple": "#ff9187"}
                    text={`Change view to ${productsViewMode==="cards" ? "table" : "cards"}`}
                    onClick={changeProductsView}
                />
            </div>
            
            <div className={productsViewMode==="cards" ? "cardsWrapper" : "cardsWrapperTableView"}>
                {products.map((product) => <ProductCard
                                                key={product.setNumber}
                                                updateSelectedProductsCount={updateSelectedProductsCount}
                                                selectedProductsId={selectedProductsId.current}
                                                id={product.id}
                                                name={product.name}
                                                price={product.price}
                                                description={product.description}
                                                color={product.color}
                                                imgUrl={product.imgUrl}
                                                openAddModal={openAddModal}
                                            />)}
                {isAddModalOpen &&
                    <Modal
                    backgroundColor='linear-gradient(to bottom, #348a0c 33%, #269618 24% 100%)'
                    header='Do you want to add this product to your cart?'
                    closeButton='true'
                    // closeButton='false'
                    text='If you choose No you, you can still add it later'
                    actions={<><button className='buttonForAddModal' onClick={handleAddToCart}>Add</button>
                    <button className='buttonForAddModal' onClick={closeModal}>No</button></>}
                    closeModal={closeModal}
                />
                }
            
                <div className="modalButtonsWrapper" style={{display: 'none'}}>
                    <Button
                        backgroundColor='#E74C3D'
                        text='Open first modal'
                        // onClick={openDeleteModal}
                    />
                    <Button
                        backgroundColor='#269618'
                        text='Open second modal'
                        onClick={openAddModal}
                    />
                </div>
            </div>
        </>
    );
}

export default Home;