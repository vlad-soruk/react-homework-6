import { useContext, useEffect, useRef, useState } from 'react';
import ProductCard from '../../components/productCard/ProductCard';
import Modal from '../../components/Modal';
import { useDispatch, useSelector } from 'react-redux';
import { closeDeleteModalAction, openDeleteModalAction } from '../../store/modalsReducer';
import FormToBuyProduct from '../../components/formToBuyProduct/FormToBuyProduct';
import Button from '../../components/Button';
import { ProductsView } from '../../App';

function Cart({updateAddedToCartProductsCount}){
    // Використовуємо useContext для того, щоб компонент міг використовувати необхідні для
    // реалізації відображення карток різним способом дані 
    const {productsViewMode, changeProductsView} = useContext(ProductsView)

    // За замовчуванням вказуємо, що стан коли немає доданих у кошик товарів дорівнює false
    let [noAddedToCartProducts, setNoAddedToCartProducts] = useState(false)
    let [productsInCart, setProductsInCart] = useState([]);
    // Тут зберігатимемо айді продуктів, доданих в кошик
    const addedToCartProductsId = useRef([]);
    const deleteFromCartProductsId = useRef([]);


    const isDeleteModalOpen = useSelector(state => state.modals.deleteModalState)
    const dispatch = useDispatch();

    // Використовуємо хук useRef щоб зберігати значення скролу сторінки
    // useState ми не можемо використати, бо функція setState має асихронну природу,
    // тому ми отримуватимемо старі (минулі) значення scrollY, тоді як useRef
    // дозволяє зберігати тільки актуальне значення
    const scrollY = useRef(0); 

    // Функція видалення зі списку productsInCart усіх товарів,
    // яка передається компоненту форми FormToBuyProduct
    function deleteAllProductsInCart(){
        setProductsInCart([])
        updateAddedToCartProductsCount()
    }

    function deleteProductFromCart(){
        closeModal();
        // Якщо користувач вирішив видалити товар з кошику, фільтруємо список айді,
        // що знаходиться в addedToCartProductsId.current та оновлюємо список айді доданих до кошика
        // товарів в локальному сховищі 
        let updatedAddedToCartProductsId = [...addedToCartProductsId.current].filter(id => id!==deleteFromCartProductsId.current);
        localStorage.setItem('addedToCartProducts', JSON.stringify(updatedAddedToCartProductsId));
        console.log('PRODUCT ID TO DELETE!!!', deleteFromCartProductsId.current);
        // Змінюємо стан productsInCart чим викликаємо ререндер сторінки
        let updatedProductsInCart = productsInCart.filter(product => product.id !== deleteFromCartProductsId.current);
        setProductsInCart(updatedProductsInCart);
        // Викликаємо функцію, що оновлює кількість товарів, доданих у кошик, що показується на сторінці
        updateAddedToCartProductsCount();
    }

    function openDeleteModal(event, id){
        scrollY.current = window.scrollY;
        dispatch(openDeleteModalAction())
        // setIsDeleteModalOpen(true);
        document.body.style.position = 'fixed';
        // Важливо вказати, що ширина дорівнюватиме 100%,
        // інакше ширина body буде меншою, ніж сам екран
        document.body.style.width = '100%';
        // Вказуємо, що box-sizing = 'border-box'
        // для того, щоб падінги враховувались у ширині body
        document.body.style.boxSizing = 'border-box';
        document.body.style.top = `-${scrollY.current}px`;
        document.body.style.paddingRight = '17px';
        
        //Зберігаємо айді товару, на якому визвали модальне вікно додавання товару в кошик
        deleteFromCartProductsId.current = id;
        console.log('addedToCartProductsId.current', addedToCartProductsId.current);
    }

    function closeModal(){
        dispatch(closeDeleteModalAction())
        // setIsDeleteModalOpen(false);
        document.body.style.position = '';
        document.body.style.top = ``;
        document.body.style.width = '';
        document.body.style.boxSizing = '';
        document.body.style.top = `-${scrollY.current}px`;
        window.scrollTo(0, scrollY.current);
        document.body.style.paddingRight = '';
    }

    useEffect(()=>{
        let addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts'));
        if ( !addedToCartProducts || addedToCartProducts.length === 0 ){
            setNoAddedToCartProducts(true)
            return
        }

        fetch('products.json')
        .then(res => res.json())
        .then(data => {
            // Додаємо у змінну productsInCart лише ті товари, айді яких присутнє в
            // масиві addedToCartProductsId
            setProductsInCart([...data.products].filter(product=>
                addedToCartProductsId.current.includes(product.id)
            ))
        })
        .catch(error => console.log('An error occured while fetching all products: ', error))
    }, [])

    useEffect(()=>{
        let addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts'));
        addedToCartProductsId.current = addedToCartProducts;
        // Якщо немає доданих у вибране товарів, то змінюємо стан noSelectedProducts на true
        if( !addedToCartProducts || addedToCartProducts.length === 0 ){
            setNoAddedToCartProducts(true)
        }
        console.log("addedToCartProductsId: ", addedToCartProductsId.current);
        // Важливим є додавання списку productsInCart в список залежностей useEffect,
        // оскільки, якщо цього не зробити, useEffect спрацює лише один раз та
        // в addedToCartProductsId.current буде зберігатися список айді з вже неактуального 
        // локального сховища (того який був до моменту видалення товару з обраних).
        // Але якщо додати productsInCart в залежності useEffect, ми запобігаємо цій помилці й
        // тепер addedToCartProductsId.current буде оновлюватись кожного разу, як змінюється
        // список productsInCart 
    }, [productsInCart])

    return (
        <>
            {/* Кнопка, що перемикає вигляд відображення товару */}
            <div className="changeViewButtonWrapper">
                <Button
                    backgroundColor={productsViewMode==="cards" ? "purple": "#ff9187"}
                    text={`Change view to ${productsViewMode==="cards" ? "table" : "cards"}`}
                    onClick={changeProductsView}
                />
            </div>

            <div className="cardsWrapper">
                {/* При noAddedToCartProducts === true відображаємо надпис що немає товарів*/}
                {noAddedToCartProducts
                ?   <h1 className='noProductsTitle'>There is no added to cart products yet</h1>
                :   null
                }
                {productsInCart.map(product => <ProductCard
                                                    key={product.setNumber}
                                                    id={product.id}
                                                    name={product.name}
                                                    price={product.price}
                                                    description={product.description}
                                                    color={product.color}
                                                    imgUrl={product.imgUrl}
                                                    deleteButton={true}
                                                    openDeleteModal={openDeleteModal}
                                                />)}
            
                {isDeleteModalOpen &&
                    <Modal
                    backgroundColor='linear-gradient(to bottom, #D44638 33%, #E74C3D 24% 100%)'
                    header='Do you want to delete this product from a cart?'
                    closeButton='true'
                    // closeButton='false'
                    text='If you delete the product you can still add it to a cart from home page'
                    actions={<><button className='buttonForDeleteModal' onClick={deleteProductFromCart}>Yes</button>
                    <button className='buttonForDeleteModal' onClick={closeModal}>No</button></>}
                    closeModal={closeModal}
                />
                }
                {!noAddedToCartProducts ? <FormToBuyProduct deleteAllProductsInCart={deleteAllProductsInCart}/> : null}
            </div>
        </>
    )
}

export default Cart