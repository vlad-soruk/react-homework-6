import { createContext, useEffect, useState } from 'react';
import './App.scss'; 
import Header from './components/header/Header';
import { Route, Routes } from 'react-router-dom';
import Cart from './pages/cart/Cart';
import Home from './pages/home/Home';
import Favorites from './pages/favorites/Favorites';

// Створюємо контекст відображення карток - через карточки або через тиблицю
export const ProductsView = createContext()
export const CurrentPage = createContext()

function App() {
    const [productsViewMode, setProductsViewMode] = useState("cards")
    const [currentPage, setCurrentPage] = useState("")

    // Функція для зміни вигляду відображення карточок
    function changeProductsView(){
        setProductsViewMode(productsViewMode === "cards" ? "table" : "cards")
    }

    // Функція для зміни currentPage
    function changeCurrentPage(page){
        setCurrentPage(page)
    }

    // Створюємо стан для числа вибраних користувачем товарів, яке потім передаватимемо
    // в компонент Header у вигляді пропсів. Аналогічно діємо з доданими в кошик товарами
    const [selectedProductsCount, setSelectedProductsCount] = useState(0);
    const [addedToCartProductsCount, setAddedToCartProductsCount] = useState(0);

    useEffect(() => {
        const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts')) || [];
        const addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts')) || [];
        setSelectedProductsCount(selectedProducts.length);
        setAddedToCartProductsCount(addedToCartProducts.length);
    }, []);

    // Функція для оновлення кількості обраних товарів, що визиватиметься компонентом,
    // якому ця функція передається
    function updateSelectedProductsCount() {
        const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts')) || [];
        setSelectedProductsCount(selectedProducts.length);
    }

    // Функція для оновлення кількості доданих у кошик товарів
    function updateAddedToCartProductsCount() {
        const addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts')) || [];
        setAddedToCartProductsCount(addedToCartProducts.length);
    }

    return (
        <ProductsView.Provider value={{productsViewMode, changeProductsView}}>
            <CurrentPage.Provider value={{currentPage, changeCurrentPage}}>
                <div className='App' >
                    <Header selectedProductsCount={selectedProductsCount}
                            addedToCartProductsCount={addedToCartProductsCount}
                    />
                    <Routes>
                        <Route path='/cart' element={<Cart
                                                    updateAddedToCartProductsCount={updateAddedToCartProductsCount}
                                                />}/>
                        <Route path='/favorites' element={<Favorites
                                                    updateSelectedProductsCount={updateSelectedProductsCount}
                                                />}/>
                        <Route path='/' element={<Home
                                                    updateSelectedProductsCount={updateSelectedProductsCount}
                                                    updateAddedToCartProductsCount={updateAddedToCartProductsCount}
                                                />}/>
                        <Route path='/*' element={<Home
                                                    updateSelectedProductsCount={updateSelectedProductsCount}
                                                    updateAddedToCartProductsCount={updateAddedToCartProductsCount}
                                                />}/>
                    </Routes>
                </div>
            </CurrentPage.Provider>
        </ProductsView.Provider>
    );
}

export default App;
